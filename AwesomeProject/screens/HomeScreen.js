import React from 'react';
import {
  AsyncStorage,
  ActivityIndicator,
  FlatList,
  Text,
  View,
  RefreshControl,
  TouchableHighlight
} from 'react-native';
import { List, ListItem } from 'react-native-elements';
function parseJSON(response) {
  return new Promise((resolve) => response.json()
    .then((json) => resolve({
      status: response.status,
      ok: response.ok,
      pagingHeader: response.headers.get('Paging-Headers'),
      json,
    })));
}
export default class HomeScreen extends React.Component {
  state = {
    data: [],
    page: 0,
    loading: false,
    nextPage: true,
    refreshing: false
  };
  static navigationOptions = {
    header: null,
  };
  componentWillMount() {
    this.fetchData();
  }
  fetchData = async () => {
    this.setState({ loading: true });
    fetch(
      `http://10.0.2.2:81/api/ListElements/Browse?pageNumber=${this.state.page}&pageSize=7`, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + await AsyncStorage.getItem('userToken')
        }
      }
    )
      .then(parseJSON)
      .then(response => {
        if (response.ok) {
          console.log(response.pagingHeader);
          const str = String(response.pagingHeader);
          console.log(str.includes('"nextPage":"Yes"'))
          this.setState(state => ({
            nextPage: str.includes('"nextPage":"Yes"')
          }));
          return response.json;
        }

        throw response.status;
      })
      .then(json => {
        this.setState(state => ({
          data: [...state.data, ...json],
          loading: false,
        }));
      })
      .catch(err => {
        console.log(err);

        if (err == 401) {
          AsyncStorage.setItem('userToken', null)
          this.props.navigation.navigate('Auth');
        } else {
          alert("Try again later");
        }
      });
  };
  handleEnd = () => {
    if (this.state.nextPage && !this.state.refreshing) {
      this.setState(state => ({ page: state.page + 1 }), () => this.fetchData());
    }
  };
  _onRefresh = () => {
    this.setState({
      data: [],
      refreshing: true,
      page: 0,
    }, () => {
      this.fetchData().then(() => {
        this.setState({ refreshing: false });
      });
    });
  }
  _onPressButton= (color) =>{
    alert('Color hash is:\n' + color);
  }
  render() {
    return (
      <View>
        <List>
          <FlatList
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
            data={this.state.data}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={() => this.handleEnd()}
            onEndReachedThreshold={0.03}
            ListFooterComponent={() =>
              this.state.loading
                ? <ActivityIndicator size="large" animating /> : null}
            renderItem={({ item }) =>
              <ListItem
                title={item.colorName}
                titleStyle={{ color: 'black', fontWeight: 'bold' }}
                subtitle={
                  <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text>Author: </Text>
                    <Text style={{ color: 'black', fontStyle: 'italic' }}>{item.creatorUserName}</Text>
                  </View>
                }

                rightIcon={
                  <TouchableHighlight onPress={()=>this._onPressButton(item.colorHex)}>
                  <View style={{
                    backgroundColor: String(item.colorHex),
                    width: 200,
                    height: 80,
                  }} />
                  </TouchableHighlight>
                }
              />
            }
          />
        </List>
      </View>
    );
  }

}

