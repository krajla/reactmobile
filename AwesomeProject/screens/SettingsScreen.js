import React from 'react';
import {
    AsyncStorage,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,

} from 'react-native'
import { ExpoConfigView } from '@expo/samples';

export default class SettingsScreen extends React.Component {
    static navigationOptions = {
        title: 'Settings',
    };

    render() {
        /* Go ahead and delete ExpoConfigView and replace it with your
        * content, we just wanted            to give you a quick view of your config */
        //return <ExpoConfigView />;
        return (
            <View>
                <TouchableHighlight onPress={this._logOut} underlayColor='#99d9f4'>
                    <Text>Logout</Text>
                </TouchableHighlight>
            </View>
        )
    }
    _logOut = async () => {
        AsyncStorage.removeItem("userToken");
        this.props.navigation.navigate('Auth');
    }
}
