import React from 'react'
import {
    AsyncStorage,
    StyleSheet,
    Text,
    View,
    TouchableHighlight,

} from 'react-native'
var t = require('tcomb-form-native');
var Form = t.form.Form;
var LoginForm = t.struct({
    username: t.String,
    password: t.String,
});
var RegisterForm = t.struct({
    username: t.String,
    password: t.String,
    confirmPassword: t.String
});
const options = {
    fields: {
        password: {
            password: true,
            secureTextEntry: true
        },
        confirmPassword: {
            password: true,
            secureTextEntry: true
        }
    }
};
function parseJSON(response) {
    return new Promise((resolve) => response.json()
        .then((json) => resolve({
            status: response.status,
            ok: response.ok,
            json,
        })));
}
export default class SignInScreen extends React.Component {
    state = {
        firstButtonText: "Log in",
        secondButtonText: "Register",
        formType: LoginForm,
        title: "Login"
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <Text style={styles.title}>{this.state.title}</Text>
                </View>
                <View style={styles.row}>
                    <Form
                        ref="form"
                        type={this.state.formType}
                        options={options}
                    />
                </View>
                <View style={styles.row}>
                    <TouchableHighlight style={styles.button} onPress={this._firstButton} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>{this.state.firstButtonText}</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button} onPress={this._secondButton} underlayColor='#99d9f4'>
                        <Text style={styles.buttonText}>{this.state.secondButtonText}</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }

    fetchBody(details) {
        let formBody = [];
        for (let property in details) {
            let encodedKey = encodeURIComponent(property);
            let encodedValue = encodeURIComponent(details[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");
        return formBody
    }

    _firstButton = async () => {
        var value = this.refs.form.getValue();

        if (value) {
            if (this.state.formType == LoginForm) {
                fetch("http://10.0.2.2:81/token", {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Accept': 'application/json',
                    },
                    body: this.fetchBody({
                        grant_type: "password",
                        username: value.username,
                        password: value.password,
                    })
                })
                    .then(parseJSON)
                    .then(response => {
                        if (response.ok) {
                            return response.json;
                        }
                        // extract the error from the server's json
                        throw response.json.error_description;
                    })
                    .then((responseData) => {
                        AsyncStorage.setItem('userToken', responseData.access_token)
                        this.props.navigation.navigate('Main');
                    })
                    .catch(err => alert(err))
                    .done();
            } else {
                fetch("http://10.0.2.2:81/api/Account/Register", {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        userName: value.username,
                        password: value.password,
                        confirmPassword: value.confirmPassword
                    })
                })
                    .then(parseJSON)
                    .then(response => {
                        if (response.ok) {
                            alert("New Account Registered, log in!");
                            this.setState(state => ({
                                firstButtonText: "Log in",
                                secondButtonText: "Register",
                                formType: LoginForm,
                                title: "Login"
                            }));
                        }                        
                        throw response.json;
                    })
                    .catch(body =>{
                        let errorMsg = '';
                        for (let property in (body.modelState)) {
                            errorMsg += body.modelState[property] + '\n'
                        }
                        alert(errorMsg);
                    })
                    .done();

            }

        }
    };
    _secondButton = async () => {
        if (this.state.formType == LoginForm) {
            this.setState(state => ({
                firstButtonText: "Create Account",
                secondButtonText: "Cancel",
                formType: RegisterForm,
                title: "Register",
            }));
        } else {
            this.setState(state => ({
                firstButtonText: "Log in",
                secondButtonText: "Register",
                formType: LoginForm,
                title: "Login"
            }));
        }

    }
}
var styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    title: {
        fontSize: 30,
        alignSelf: 'center',
        marginBottom: 30
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    button: {
        height: 36,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
});