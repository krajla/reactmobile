import React from 'react';
import { View, Text, AsyncStorage } from 'react-native'
import { TriangleColorPicker } from 'react-native-color-picker'
import DialogInput from 'react-native-dialog-input';

export default class UploadScreen extends React.Component {
  state = {
    isDialogVisible: false,
    color: null,
  };
  handleColorChanged = (color) => {
    this.setState(state => ({
      isDialogVisible: true,
      color: color
    }));
  };
  handleSumbitColor = async (name) => {
    fetch(
      `http://10.0.2.2:81/api/ListElements/Upload`, {
        method: "POST",
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + await AsyncStorage.getItem('userToken')
        },
        body: JSON.stringify({
          colorHex: this.state.color,
          colorName: name
        }),
      }
    )
      .then(response => {
        console.log(response.status);
        if (response.status == 401) {
          AsyncStorage.setItem('userToken', null)
          this.props.navigation.navigate('Auth');
        }
      })
      .catch(err=>alert(err));
    this.handleClose();
  };
  handleClose = () => {
    this.setState(state => ({
      isDialogVisible: false
    }));
  };
  render() {
    return (
      <View style={{ flex: 1, padding: 15}}>
        <TriangleColorPicker
          onColorSelected={(color) => this.handleColorChanged(color)}
          style={{ flex: 1 }}
        />
        <DialogInput isDialogVisible={this.state.isDialogVisible}
          title={"Sumbit Color"}
          message={"Sumbit your awesome color for everyone to see 😁✌👌👌😜"}
          hintInput={"Color name"}
          submitInput={(inputText) => this.handleSumbitColor(inputText)}
          closeDialog={() => this.handleClose()}
        >
        </DialogInput>
      </View>
    )
  }
}